import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage.js';
import Nav from './Nav.js';
import ShoeForm from './ShoeForm.js';
import ListShoeForm from './ListShoeForm.js';

function App(props) {
  if (props.hats === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="new" elemenet={<ShoeForm />} />
          <Route path="" element={<ListShoeForm />} />
          <Route path="new" element={<HatForm />} />
          <Route path="" element={<HatsList hats={props.hats} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
