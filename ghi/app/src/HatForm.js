import React, { useEffect, useState } from 'react';


class HatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            style_name: '',
            fabric: '',
            color: '',
            picture_url: '',
            location: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeStyleName = this.handleChangeStyleName.bind(this);
        this.handleChangeFabric = this.handleChangeFabric.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this)
        this.handleChangePictureURL = this.handleChangePictureURL.bind(this);
        this.handleChangeLocation = this.handleChangeLocation.bind(this);
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.locations;

        const hatURL = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(hatURL, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();

            const cleared = {
                style_name: '',
                fabric: '',
                color: '',
                picture_url: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    handleChangeStyleName(event) {
        const value = event.target.value;
        this.setState({ style_name: value });
    }

    handleChangeFabric(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handleChangePictureURL(event) {
        const value = event.target.value;
        this.setState({ picture_url: value });
    }

    handleChangeLocation(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }
}

return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={this.handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input
                            value={this.state.style_name}
                            onChange={this.handleChangeStyleName}
                            placeholder="Style Name"
                            required
                            type="text"
                            name="style_name"
                            id="style_name"
                            className="form-control"
                        />
                        <label htmlFor="style_name"> Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={this.state.fabric}
                            onChange={this.handleChangeFabric}
                            placeholder="Fabric"
                            required
                            type="text"
                            name="fabric"
                            id="fabric"
                            className="form-control"
                        />
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={this.state.color}
                            onChange={this.handleChangeColor}
                            placeholder="Color"
                            required
                            type="text"
                            name="color"
                            id="color"
                            className="form-control"
                        />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={this.state.picture_url}
                            onChange={this.handleChangePictureURL}
                            placeholder="Picture URL"
                            required
                            type="textarea"
                            name="picture_url"
                            id="picture_url"
                            className="form-control"
                        />
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select
                            value={this.state.location}
                            onChange={this.handleChangeLocation}
                            required
                            id="location"
                            className="form-select"
                            name="location"
                        >
                            <option value="">Choose a location</option>
                            {this.state.locations.map((location) => {
                                return (
                                    <option key={location.href} value={location.href}>
                                        {location.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
);

export default HatForm;
