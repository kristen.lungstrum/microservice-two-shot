import { React, useEffect, useState } from "react";

function ShoeForm() {
    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const [m_name, setMname] = useState('');
    const handleMnameChange = (event) => {
        const value = event.target.value;
        setMname(value);
    }
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const [picture_url, setpicture_url] = useState('');
    const handlepicture_urlChange = (event) => {
        const value = event.target.value;
        setpicture_url(value);
    }
    const [bin, setBin] = useState('');
    const handlebinsChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }
    const [bins, setBins] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.m_name = m_name;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;
        console.log(data);

        const shoesUrl = 'http://localhost:8080/shoes/api/create';
        const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
            const response = await fetch(shoesUrl, fetchConfig);
            if (response.ok) {
                setMname('');
                setManufacturer('');
                setColor('');
                setpicture_url('');
                setBin('');
            }
        }
    // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
          // , {mode: 'no-cors'});
        if (response.ok) {
            // const response = await fetch(url);
            const data = await response.json();
            setBins(data.bins);
            //console.log(bins);
            //console.log(fetchData);
        }
    }
    useEffect(() => {
        fetchData();
        }, [] );

        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleMnameChange} placeholder="Mname" required type="text" name="Mname" value={m_name} id="Mname" className="form-control"/>
                    <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="Manufacturer" value={manufacturer} id="Manufacturer" className="form-control"/>
                    <label htmlFor="Manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} placeholder="Color" required type="text" name="Color" id="Color" value={color} className="form-control"/>
                    <label htmlFor="Color">Color</label>
                    </div>
                    <div className="mb-3">
                    <input onChange={handlepicture_urlChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" value={picture_url} className="form-control"/>
                    <label htmlFor="picture_url">Picture_url</label>
                    </div>
                    <div className="form-floating mb-3">
                    <select onChange={handlebinsChange} required name="bins" id="bins" value={bin.import_href} className="form-select">
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return (
                        <option key={bin.href} value={bin.id}>{bin.closet_name} - {bin.bin_number}</option>
                    );
                    })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create a shoe</button>
                    </form>
                    </div>
                </div>
                </div>
                );
                }

        export default ShoeForm;
