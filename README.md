# Wardrobify

Team:

* Preeti Mahar - shoes
* Kristen Lungstrum - hats

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

* Shoes model illustrates the details of the shoe and is used along with the list and detail encoders to list, create and delete or update shoes.
* Shoes poller is a poller to poll the wardrobe api for Bin resources.
to pull Bin data from the api.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

* Hat model illustrates the details of the hat and is used along with the encoders to list, create and delete hats.
* Hat poller is a poller to poll the wardrobe api for Location resources
to pull Location data from the api.
